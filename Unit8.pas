unit Unit8;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Math, Vcl.StdCtrls, block;

type

  TcurrentLCOE = function (block:Tblock;discountRate:double;fuelData:TfuelData;newCapitalCost:double)
  :double of object;

  TForm8 = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
  private
     currentLCOEFunc:TcurrentLCOE;
    { Private declarations }
  public
    function GetLCOEVar1(block:Tblock;discountRate:double;fuelData:TfuelData;newCapitalCost:double):
        double;
    function GetLCOEVar2(block:Tblock;discountRate:double;fuelData:TfuelData;newCapitalCost:double):
        double;
    function GetLCOEVar3(block:Tblock;discountRate:double;fuelData:TfuelData;
        newCapitalCost:double): double;



    { Public declarations }
  end;

var
  Form8: TForm8;


implementation

{$R *.dfm}



procedure TForm8.FormCreate(Sender: TObject);
begin

var bufBlock:Tblock;
var blocks:Tarray<Tblock>:=[];


bufBlock.name:='���(5-40)';
bufBlock.power:=100;
bufBlock.lifeTime:=25;
bufBlock.efficiency:=0.35;
bufBlock.loadFactor:=0.4;
bufBlock.fixedOperationCost:=22620;
bufBlock.varOperationCost:=6.264;

blocks:=blocks+[bufBlock];


bufBlock.name:='���(40-125)';
bufBlock.power:=125;
bufBlock.lifeTime:=25;
bufBlock.efficiency:=0.40;
bufBlock.loadFactor:=0.4;
bufBlock.fixedOperationCost:=22620;
bufBlock.varOperationCost:=5.104;

blocks:=blocks+[bufBlock];

bufBlock.name:='���(10-100)';
bufBlock.power:=100;
bufBlock.lifeTime:=25;
bufBlock.efficiency:=0.48;
bufBlock.loadFactor:=0.65;
bufBlock.fixedOperationCost:=22620;
bufBlock.varOperationCost:=5.104;

blocks:=blocks+[bufBlock];



bufBlock.name:='���(100-500)';
bufBlock.power:=500;
bufBlock.lifeTime:=25;
bufBlock.efficiency:=0.57;
bufBlock.loadFactor:=0.65;
bufBlock.fixedOperationCost:=22620;
bufBlock.varOperationCost:=5.104;

blocks:=blocks+[bufBlock];









var fuelData:TfuelData;
var discountRates,capitalCost1,capitalCost2,capitalCost3,capitalCost4:Tarray<double>;
var lcoe1,lcoe2,lcoe3:double;

discountRates:=[0.03,0.05,0.07,0.1];
capitalCost1:= [700,800,900,1050,1200];
capitalCost2:= [460,600,700,800,950,1050];
capitalCost3:= [1300,1400,1500,1600,1750,1850,1900,2100];
capitalCost4:= [900,1050,1200,1300,1400];
//capitalCost4:= [400,900,1200,1300,1400];


fuelData.startPrice:=300;
//fuelData.finishPrice:=300;


Label1.Caption:='';
Label2.Caption:='';
Label3.Caption:='';


self.currentLCOEFunc:=self.GetLCOEVar3;


Label3.Caption:=Label3.Caption + blocks[0].name+#13;
for var j := 0 to high(capitalCost1) do

begin
      Label3.Caption:=Label3.Caption + capitalCost1[j].ToString+'$/���  ';
  for var I := 0 to 3 do
  begin

    lcoe3:=self.currentLCOEFunc(blocks[0],discountRates[i],fuelData,capitalCost1[j]);

    Label3.Caption:=Label3.Caption + FloatToStrF(lcoe3,ffFixed,8,2)+ ' ';
  end;

  Label3.Caption:=Label3.Caption +#13;

end;


Label3.Caption:=Label3.Caption + blocks[1].name+#13;
for var j := 0 to high(capitalCost2) do

begin
      Label3.Caption:=Label3.Caption + capitalCost2[j].ToString+'$/���  ';
  for var I := 0 to 3 do
  begin

    lcoe3:=self.currentLCOEFunc(blocks[1],discountRates[i],fuelData,capitalCost2[j]);

    Label3.Caption:=Label3.Caption + FloatToStrF(lcoe3,ffFixed,8,2)+ ' ';
  end;

  Label3.Caption:=Label3.Caption +#13;

end;

Label3.Caption:=Label3.Caption + blocks[2].name+#13;
for var j := 0 to high(capitalCost3) do

begin
      Label3.Caption:=Label3.Caption + capitalCost3[j].ToString+'$/���  ';
  for var I := 0 to 3 do
  begin

    lcoe3:=self.currentLCOEFunc(blocks[2],discountRates[i],fuelData,capitalCost3[j]);

    Label3.Caption:=Label3.Caption + FloatToStrF(lcoe3,ffFixed,8,2)+ ' ';
  end;

  Label3.Caption:=Label3.Caption +#13;

end;

Label3.Caption:=Label3.Caption + blocks[3].name+#13;
for var j := 0 to high(capitalCost4) do

begin
      Label3.Caption:=Label3.Caption + capitalCost4[j].ToString+'$/���  ';
  for var I := 0 to 3 do
  begin

    lcoe3:=self.currentLCOEFunc(blocks[3],discountRates[i],fuelData,capitalCost4[j]);

    Label3.Caption:=Label3.Caption + FloatToStrF(lcoe3,ffFixed,8,2)+ ' ';
  end;

  Label3.Caption:=Label3.Caption +#13;

end;



///
end;





function TForm8.GetLCOEVar1(block:Tblock;discountRate:double;fuelData:TfuelData;
    newCapitalCost:double): double;
begin

const gasConvertValue = 1.136;

var fuelPrice:Tarray<double>;

var fuelStep := (fuelData.finishPrice - fuelData.startPrice)*gasConvertValue/(block.lifetime-1);

fuelPrice:=fuelPrice + [fuelData.startPrice*gasConvertValue];
for var I := 1 to block.lifetime-1 do
  fuelPrice:= fuelPrice+[fuelPrice[i-1]+fuelStep];

var totalCapitalCost := block.GetTotalCapitalCost;


//////////////////////////////////////////////////


var energyForOneYear := block.GetTotalEnergyForOneYear;

var discountedEnergyArray:Tarray<double>;

for var I := 0 to block.lifeTime-1 do
  disCountedEnergyArray:=disCountedEnergyArray+[energyForOneYear/power(1+discountRate,i)];
//  disCountedEnergyArray:=disCountedEnergyArray+[energyForOneYear/power(1+discountRate,i+1)];


var discountedOperationCost:Tarray<double>;


for var I := 0 to block.lifetime-1 do
  discountedOperationCost:=discountedOperationCost+[

  (  (0.05*totalCapitalCost)/block.power/8760) +
   discountedEnergyArray[i]*(0.123/block.efficiency)*fuelPrice[i] /power(1+discountRate,i)
  ];


var totalDiscountedOperationCost := sum(discountedOperationCost);

var totalDiscountedEnergy := sum(disCountedEnergyArray);


result:=(totalCapitalCost+totalDiscountedOperationCost)/totalDiscountedEnergy


///
end;

function TForm8.GetLCOEVar2(block:Tblock;discountRate:double;fuelData:TfuelData;
    newCapitalCost:double): double;
begin

var totalCapitaCost:=block.GetTotalCapitalCost;

var a:=(totalCapitaCost*discountRate)/(1-power(1+discountRate,-block.lifeTime));

var totalEnergy := block.GetTotalEnergyForOneYear;

var b := a/totalEnergy;

//var c := a/b;

//var fixedPart := block.fixedOperationCost/8760;

var priceOneMwth :=(0.123/block.efficiency)*fuelData.startPrice+block.varOperationCost;

var d := b + priceOneMwth;

result:=d;


end;

function TForm8.GetLCOEVar3(block:Tblock;discountRate:double;fuelData:TfuelData;newCapitalCost:double): double;
begin

const gasConvertValue = 1.136;

var crf := (discountRate*power(1+discountRate,block.lifeTime))
                /(power(1+discountRate,block.lifeTime-1));


result:=(newCapitalCost*1000*crf+block.fixedOperationCost)/(8760*block.loadFactor)+block.varOperationCost+
(0.123/block.efficiency)*fuelData.startPrice/gasConvertValue;


end;



end.
