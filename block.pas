unit block;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Math, Vcl.StdCtrls;

type

  TfuelData = record

  startPrice,finishPrice:double;

  end;

  TBlock = record




    name:string;
    power: double;
    efficiency: double;
    varOperationCost:double;
    fixedOperationCost:double;
    loadFactor: double;
    capitalCost: double;
    lifeTime: integer;
    function GetTotalCapitalCost: double;
    function GetTotalEnergyForOneYear: double;

  end;

implementation

function TBlock.GetTotalCapitalCost: double;
begin
  Result := self.power * self.capitalCost*1000;
end;

function TBlock.GetTotalEnergyForOneYear: double;
begin

result:=self.power*self.loadFactor*8760;

end;

end.
